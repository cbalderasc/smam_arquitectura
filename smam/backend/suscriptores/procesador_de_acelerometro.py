import pika
import sys
sys.path.append('../')
from monitor import Monitor
import time
import settings

class ProcesadorAcelerometro:

    def consume(self):
        try:
            logging.basicConfig()
            # Url que define la ubicación del Distribuidor de Mensajes
            url = 'amqp://oevvxuqp:D6vn6A9ErigVUrxOINL-ok-vdD610S_I@wombat.rmq.cloudamqp.com/oevvxuqp'
            # Se utiliza como parámetro la URL dónde se encuentra el Distribuidor
            # de Mensajes
            params = pika.URLParameters(url)
            params.socket_timeout = 5
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(params)
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='accelerometer', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(self.callback, queue='accelerometer')
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexión
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

        
        def callback(self, ch, method, properties, body):
        json_message = self.string_to_json(body)
        if float(json_message['x_position']) > 10 or float(json_message['x_position']) < 0 or float(json_message['y_position']) < -10 or float(json_message['y_position']) > 2 or float(json_message['z_position']) > 1 or float(json_message['z_position']) < -10:
            monitor = Monitor()
            monitor.print_notification(json_message['datetime'], json_message['id'], json_message[
                                       'preasure'], 'Acelerometro', json_message['model'])
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == '__main__':
    p_acc = ProcesadorAcelerometro()
    p_acc.consume()